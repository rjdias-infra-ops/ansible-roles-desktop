# Ansible Roles Desktop
This repository mostly includes roles for desktop/laptop use.  
For server like roles, made by professionals, visit:
* [Bert "bertvv" Vreckem][bertvv]
* [Jeff "geerlingguy" Geerling][jeff-geerling]


## Contents:
* [container-tools](roles/container-tools/tasks/main.yml)
* [disable-firewall](roles/disable-firewall/tasks/main.yml)
* [disable-ipv6](roles/disable-ipv6/tasks/main.yml)
* [disable-selinux](roles/disable-selinux/tasks/main.yml)
* [dnsmasq-cache](roles/dnsmasq-cache/tasks/main.yml)
* [etc-hosts-hostname](roles/etc-hosts-hostname/tasks/main.yml)
* [etc-hosts-inventory](roles/etc-hosts-inventory/tasks/main.yml)
* [fedora-dnf-template](roles/fedora-dnf-template/tasks/main.yml)
* [fedora-nvidia-negativo17](roles/fedora-nvidia-negativo17/tasks/main.yml)
* [fedora-rpm-fusion](roles/fedora-rpm-fusion/tasks/main.yml)
* [grub-oem](roles/grub-oem/tasks/main.yml)
* [inventory-hostname](roles/inventory-hostname/tasks/main.yml)
* [ssh-template](roles/ssh-template/tasks/main.yml)
* [upgrade-packages](roles/upgrade-packages/tasks/main.yml)


## How to use:
* Clone the repository (duh).  
* Add the full path to the `ansible-roles-desktop/roles` directory to your `roles_path` variable on your `ansible.cfg` file.  
* Change your playbook variables accordingly.  

(Don't actually disable SELinux/IPv6/Firewall, unless testing)


## Example playbook:
```yaml
- name: install container tools and apply ssh template
  hosts: all
  vars:
    ssh_allow_users: ["user1", "user2"]
  roles:
    - container-tools
    - ssh-template
(...)
```

## Tested on:
* Fedora Workstation 30


## Resources:
[Ansible Documentation (latest)](https://docs.ansible.com/ansible/latest/)  
[Ansible Roles Search Path](https://docs.ansible.com/ansible/latest/user_guide/playbooks_reuse_roles.html#role-search-path)

[bertvv]: https://github.com/bertvv?tab=repositories
[jeff-geerling]: https://github.com/geerlingguy?tab=repositories

